import sqlite from 'sqlite'
import SQL from 'sql-template-strings';

const initializeDatabase = async () => {

  const db = await sqlite.open('./db.sqlite');
  
  /**
   * retrieves the contacts from the database
   */
  const getmembersList = async () => {
    const rows = await db.all("SELECT members_id AS id, name, position, image FROM members")
    return rows
  }
  
  const controller = {
    getmembersList
  }

  return controller
}

export default initializeDatabase
