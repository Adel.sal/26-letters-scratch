import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends React.Component {
  state = { members_list: [] }
  componentDidMount(){
    const getList = async()=>{
        const response = await fetch('//localhost:8080/members/list')
        const data = await response.json()
        this.setState({members_list:data})
    }
    getList();
  }
  
  
  render() {
    const { members_list } = this.state

    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          { members_list.map( members => 
                <div key={members.id}>
                  <p>{members.id} -  {members.name}- {members.position} - {members.image}</p>
                </div>
            )
          }
          <p>
            Edit <code>src/App.js</code> and save to reload.
          </p>
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React
          </a>
        </header>
      </div>
    );
  }
}

export default App;
